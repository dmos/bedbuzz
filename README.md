# BedBuzz

BedBuzz is a **responsive web application** developed to work with an electrical system created by students of electrical engineering of University of Aveiro. this project would allow to configure **sensors** of **heart rate** and **electromyography** of bedridden people with high physical limitations.

This project was developed for a class module for the **University of Aveiro**

Note: images are not of final project and with fake data due to legal reasons.

![Graph](./images/bedbuzz_values.png)


## Core Tecnologies

* HTML
* CSS
* Bootstrap
* JavaScript
* JQuery
* PHP
* Laravel
* JSON
* AJAX
* PostgreSQL
* ChartJS



## Main Functionality

* [x] CRUD Patient
* [x] CRUD Sensors
* [x] CRUD Caretakers
* [x] CRUD Health Units
* [x] CRUD Alerts
* [x] Live data of sensor
* [x] History of Values of Sensors
* [x] API Routes
* [x] etc..


## Getting Started

1. `git clone ...`
2. `cd` into folder
3. Run `composer install` to install dependencies
4. Run `php artisan key:generate` to generate unique application key
5. Change `.env` file with database credecials
6. Run `php artisan migrate` to setup the database
7. Run `php artisan db:seed` to insert default values in database
9. Copy project folder to web server
10. Enjoy! 

___

## Other fotos

![List](./images/bedbuzz_list.png)

___

## Developed By
* Diogo Santos
* Jorge Godinho
* Pedro Quinta
* Tiago Silva
* Ricardo Balreira